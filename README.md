# Golang con Docker-compose
# Pequeñas instrucciones una vez clonado

# Para ejecutarlo: 
	docker-compose up -d

# Para realizar las prácticas:

	docker-compose exec Golang sh

# Adentro de la imagen para correr y ejecutar algún script

	go run aplicacion.go
