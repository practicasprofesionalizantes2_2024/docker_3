package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	if len(os.Args) < 3 {
        fmt.Println("Uso: go run leo2.go <archivo_entrada> <archivo_salida>")
        os.Exit(1)
    }
	inputFileName := os.Args[1]
    outputFileName := os.Args[2]

    fmt.Printf("Archivo de entrada: %s\n", inputFileName)
    fmt.Printf("Archivo de salida: %s\n", outputFileName)
	file, err := os.Open(inputFileName)
	if err != nil {
		fmt.Println("Error al abrir el archivo Input", err)
		return
	}
	defer file.Close()
	outputFile, err := os.Create(outputFileName)
	if err != nil {
		fmt.Println("Error al crear el archivo de salida:", err)
		return
	}
	defer outputFile.Close()

	scanner := bufio.NewScanner(file)
	writer := bufio.NewWriter(outputFile)

	// Recorrer cada línea del archivo de entrada
	for scanner.Scan() {
		line := scanner.Text()
		// Separar los campos de la línea usando '|' como delimitador
		fields := strings.Split(line, "|")

		// Verificar que la línea tenga la estructura esperada
		if len(fields) != 15 {
			fmt.Println("Error: La línea no tiene la estructura esperada:", line)
			continue
		}
		// Seleccionar los datos deseados para el archivo de salida
		id_provincia := fields[1]
		detalle_provincia := fields[2]

		// Escribir los datos seleccionados en el archivo de salida
		_, err := writer.WriteString(fmt.Sprintf("%s|%s\n", id_provincia, detalle_provincia))
		if err != nil {
			fmt.Println("Error al escribir en el archivo de salida:", err)
			return
		}
	}
	if err := scanner.Err(); err != nil {
		fmt.Println("Error al leer el archivo de entrada:", err)
		return
	}
	if err := writer.Flush(); err != nil {
		fmt.Println("Error al escribir en el archivo de salida:", err)
		return
	}
	fmt.Println("Archivo de salida creado con éxito:", outputFileName)
}
