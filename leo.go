package main

import (
    "encoding/csv"
    "fmt"
    "os"
	"strings"
	"bufio"
)
func main() {
	if len(os.Args) < 3 {
        fmt.Println("Uso: go run leo.go <archivo_entrada> <archivo_salida>")
        os.Exit(1)
    }
	inputFileName := os.Args[1]
    outputFileName := os.Args[2]

    fmt.Printf("Archivo de entrada: %s\n", inputFileName)
    fmt.Printf("Archivo de salida: %s\n", outputFileName)
	// Abrir el archivo de entrada
    file, err := os.Open(inputFileName)
    if err != nil {
        fmt.Println("Error al abrir el archivo:", err)
        return
    }
    defer file.Close()

    reader := csv.NewReader(file)
    records, err := reader.ReadAll()
    if err != nil {
        fmt.Println("Error al leer el archivo CSV:", err)
        return
    }

    outFile, err := os.Create(outputFileName)
    if err != nil {
        fmt.Println("Error al crear el archivo de salida:", err)
        return
    }
    defer outFile.Close()
 
    writer := bufio.NewWriter(outFile)
    for _, record := range records {
        line := strings.Join(record, "|")
        _, err := writer.WriteString(line + "\n")
        if err != nil {
            fmt.Println("Error al escribir en el archivo de salida:", err)
            return
        }
    }
    err = writer.Flush()
    if err != nil {
        fmt.Println("Error al hacer flush en el archivo de salida:", err)
        return
    }

    fmt.Println("Archivo generado con éxito:", outputFileName)
}